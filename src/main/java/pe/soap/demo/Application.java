package pe.soap.demo;

import pe.gob.sunat.service.StatusResponse;
import pe.gob.sunat.servicio2.registro.electronico.comppago.consulta.ws.service.BillConsultService;
import pe.gob.sunat.servicio2.registro.electronico.comppago.consulta.ws.service.BillService;
import pe.soap.demo.resolver.HeaderHandlerResolver;

public class Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//INSTACIAMOS EL WS DE SUNAT
		BillConsultService service = new BillConsultService();
		
		//ANTES DE OBTENER EL PORT SETEAR EL HEADER DE SEGURIDAD
		HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver();
		service.setHandlerResolver(handlerResolver);
		
		//OBTENEMOS EL PORT
		BillService consultService = service.getBillConsultServicePort();
		
		//SEGUN SUNAT NOS EL GET STATUS NOS DEVLVERA UN STATUSRESPONSE
		StatusResponse response = consultService.getStatus("RUC", "TIPO_COMPROBANTE", "SERIE", 12);
		
		//PINTAMOS EN CONSOLA LOS DATOS QUE NECESITEMOS
		System.out.println(response.getStatusMessage());
		
	}

}
